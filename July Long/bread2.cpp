#include <iostream>

using namespace std;


int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	long long t, i, k, rk, breads, n, flag;
	cin>>t;

	while(t--) {
		
		cin>>n>>k;
		breads = 0;
		rk = k;	
		while(n--) {
			cin>>i;
			flag = 0;
			if(i<=rk) {
				rk -= i;
				// cout<<"E";

				if(rk==0 || rk==1) {
					rk = k;
					breads++;
					flag = 1;
					continue;
				}rk--;
				continue;
			}
			if(i > rk) {
				i -= rk;
				// cout<<"A";
				breads++;
				rk = k;
			}
			if(i >= k) {
				breads += i/k;
				i = i%k;
				// cout<<"B";
				if(i==0){
					flag = 1;
					continue;
				}
			}if(i < k) {
				
				rk -= i;
				// cout<<"C";
				if(rk==1){
					rk = k;
					breads++;
					flag = 1;
					continue;
				}
				rk--;
			} 		
		}
		if(flag)
			cout<<breads<<endl; 	
		else
			cout<<++breads<<endl; 	
	} 

	return 0;
}

