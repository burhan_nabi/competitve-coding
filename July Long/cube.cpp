#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)



int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	int t;
	int c = 0,flag = 0;

	vi A(6);
	vi count(6);
	int pos[24][3]={
		{0,2,5},{0,5,3},{0,3,4},{0,4,2},
		{1,2,5},{1,5,3},{1,3,4},{1,4,2},
		{2,1,5},{2,5,0},{2,0,4},{2,4,1},
		{3,1,5},{3,5,0},{3,0,4},{3,4,1},
		{4,2,0},{4,0,3},{4,3,1},{4,1,2},
		{5,2,0},{5,0,3},{5,3,1},{5,1,2}
	};

	string s;
	cin>>t;

	while(t--) {
	
		c=0,flag=0;
		f(i,0,6) {
			
			cin>>s;
			
			// if(s=="a")A[i]=1;
			// else if(s=="b")A[i]=2;
			// else if(s=="c")A[i]=3;
			// else if(s=="d")A[i]=4;
			// else if(s=="e")A[i]=5;
			// else A[i] = 0;

			if(s=="blue")A[i]=1;
			else if(s=="black")A[i]=2;
			else if(s=="green")A[i]=3;
			else if(s=="yellow")A[i]=4;
			else if(s=="red")A[i]=5;
			else A[i] = 0;

			count[A[i]]++;
		}


		f(i,0,24) {

			// cout<<A[pos[i][0]]<<":"<<A[pos[i][1]]<<":"<<A[pos[i][2]]<<"\t";
			// cout<<pos[i][0]<<":"<<pos[i][1]<<":"<<pos[i][2]<<endl;
			if( A[pos[i][0]]==A[pos[i][1]] && A[pos[i][0]]==A[pos[i][2]]) {
				// cout<<i;
				cout<<"YES"<<endl;
				flag = 1;
				goto exit;
			}
		}

		exit:
		if(flag)continue;
		else cout<<"NO"<<endl;

	}

	return 0;
}

