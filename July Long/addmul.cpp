#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)
long long M = 1e9+7;

long long lazy[1000005];

int inline getMid(int s, int e) { return s + (e -s)/2; }

void updateValueUtil1(long long *st, int ss, int se, int i, int diff, int index)
{
  	if(lazy[index] != 0) { // This index needs to be updated
   		st[index] += lazy[index]; // Update it
 
		if(ss != se) {
			lazy[index*2+1] += lazy[index]; // Mark child as lazy
    			lazy[index*2+2] += lazy[index]; // Mark child as lazy
		}
 
   		lazy[index] = 0; // Reset it
  	}

	if (i < ss || i > se)
		return;

	st[index] = st[index] + diff;
	// cout<<"V:"<<st[index]<<" "<<"V%M:"<<st[index]%M<<endl;
	st[index] = st[index] % M;
	
	if (se != ss)
	{
		int mid = getMid(ss, se);
		updateValueUtil1(st, ss, mid, i, diff, 2*index + 1);
		updateValueUtil1(st, mid+1, se, i, diff, 2*index + 2);
	}
}

void updateValueUtil2(long long *st, int ss, int se, int i, int mul, int index)
{
  	if(lazy[index] != 0) { // This index needs to be updated
   		st[index] += lazy[index]; // Update it
 
		if(ss != se) {
			lazy[index*2+1] += lazy[index]; // Mark child as lazy
    			lazy[index*2+2] += lazy[index]; // Mark child as lazy
		}
 
   		lazy[index] = 0; // Reset it
  	}

	if (i < ss || i > se)
		return;

	st[index] = st[index] + mul;
	st[index] = st[index] % M;
	if (se != ss)
	{
		int mid = getMid(ss, se);
		updateValueUtil2(st, ss, mid, i, mul, 2*index + 1);
		updateValueUtil2(st, mid+1, se, i, mul, 2*index + 2);
	}
}


int getSumUtil(long long *st, int ss, int se, int qs, int qe, int index)
{
	if (qs <= ss && qe >= se)
		return st[index];

	if(lazy[index] != 0) { // This index needs to be updated
		st[index] += lazy[index]; // Update it
 
		if(ss != se) {
			lazy[index*2+1] += lazy[index]; // Mark child as lazy
			lazy[index*2+2] += lazy[index]; // Mark child as lazy
		}
 
		lazy[index] = 0; // Reset it
	}



	if (se < qs || ss > qe)
		return 0;

	int mid = getMid(ss, se);
	// long long ls = getSumUtil(st, ss, mid, qs, qe, 2*index+1);
	// long long rs = getSumUtil(st, mid+1, se, qs, qe, 2*index+2);
	return getSumUtil(st, ss, mid, qs, qe, 2*index+1) +
		getSumUtil(st, mid+1, se, qs, qe, 2*index+2);	
}

int constructSTUtil(long long Orig[], int ss, int se, long long *st, int si)
{
	if (ss == se)
	{
		st[si] = Orig[ss];
		return Orig[ss];
	}

	int mid = getMid(ss, se);
	st[si] = constructSTUtil(Orig, ss, mid, st, si*2+1) +
			constructSTUtil(Orig, mid+1, se, st, si*2+2);
	return st[si];
}


long long *constructST(long long Orig[], int n)
{
	int x = (int)(ceil(log2(n)));
	int max_size = 2*(int)pow(2, x) - 1;
	long long *st = new long long[max_size];

	constructSTUtil(Orig, 0, n-1, st, 0);

	return st;
}


int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	int n, q;

	// cout<<M;

	f(i,0,1000005)lazy[i] = 0;

	cin>>n>>q;
	long long Orig[n];
	
	f(i,0,n)cin>>Orig[i];
	// f(i,0,n)Orig[i] = 1;

	long long *st = constructST(Orig, n);

	int c , x, y, v;

	while(q--)
	{

		// f(i,0,2*n)cout<<st[i]<<" ";
		// cout<<endl;

		cin>>c>>x>>y;


		if(c==4) 
		{
			// Print sum
			cout<<getSumUtil(st,0 , n-1, x-1, y, 0)<<endl;
		}
		else
		{
			cin>>v;
			switch(c)
			{
				case 1:
					f(i,x-1,y)
					{
						updateValueUtil1(st, 0, n-1, i, v, 0);
						Orig[i] += v;
						Orig[i] %= M;
					}
					break;
				case 2:
					f(i,x-1,y)
					{
						int O = Orig[i];
						long long t = Orig[i];
						t *= v;
						t = t - O;
						t %= M;
						if(t<0)t+=M;
						updateValueUtil2(st, 0, n-1, i, t, 0);
						Orig[i] = t + O;
						Orig[i] %= M;
					}
					break;
				case 3:
					f(i,x-1,y)
					{
						int d = v - Orig[i];
						d %= M;
						if(d<0)d%=M;
						// cout<<"d:"<<d<<endl;
						updateValueUtil1(st, 0, n-1, i, d, 0);
						Orig[i] = v;
					}
					break;
			}
		}
	}

	return 0;
}

