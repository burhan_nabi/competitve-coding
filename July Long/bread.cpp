#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)

long long tc, t, n, k, rk , breads = 0, c = 1 ,flag = 1;

void solve() {

	breads = 0, c = 1 ,flag = 1;	
	cin>>n>>k;
	cin>>t;
	
	goto start;

	start:
		if(flag){
			flag=0;
			rk = k;
			// cout<<"yum"<<endl;
		}
		if( t > rk) {
			t -= rk;
			breads++;
			if( t>k ) {
				breads += t/k;
				t = t%k;
			}
			flag = 1;
			goto start;
		}
		else {
			rk -= t;
			if(c == n){
				// breads++;
				goto exit;
			}

			cin>>t;
			c++;

			if(rk==0){
				flag = 1;
				breads++;
				goto start;
			}
			else rk--;
			goto start;
		}
	exit:	
	breads++;
	cout<<breads<<endl;
}


int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	
	
	cin>>tc;

	while(tc--) {

		solve();
	
	}
	return 0;
}

