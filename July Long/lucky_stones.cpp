#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)



int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	int n, count;
	
	long long t, temp;

	cin>>n;
	while(n--) {

		cin>>t;
		if(t%10==0) {
			beg:
			count = 0;
			temp = t;
			while(t%10==0) {
				count++;
				t /= 10;
			}
			if(t%10==5) {
				t = temp*4;
				goto beg;
				cout<<temp*4<<endl;
			}else {
				cout<<temp<<endl;
			}
		}else if( t%10==5) {
			goto beg;
		}else {
			cout<<t<<endl;
		}

	}


	return 0;
}

