#include <stdio.h>
#include <stdlib.h>

int inline max(int a, int b) {return (a>b)?a:b;}

int longIncSeq( int a[], int n);


int main(int argc, char const *argv[])
{
	int t, n, i, j, q, maxN, ind;


	scanf( "%d", &t );

	while( t-- ) { 
		
		scanf( "%d", &n );
		
		int ar[10001]={0}, final[10001]={0}, from[10001]={0};
		int min = 10000000, min_ind = -1;
		

		for( i=1; i<=n; i++ ) {

			scanf( "%d", &ar[i] );
			if(ar[i]<min) {
				min = ar[i];
				min_ind = i;
			}
		}


		for( i=1; i<=n; i++ ) {
			
			j = (min_ind%n);
			if(j==0){
				final[i] = ar[n];
				min_ind++;
				continue;
			} 
			final[i] = ar[j];
			min_ind++;
		}

		for( i=1; i<=n; i++ ) {

			printf("%d ",final[i]);
		}printf("\n");

		ar[1] = 1,from[1] = 0, maxN = 1;

		for( i=2; i<=n; i++ ) {
			
			q = -1;
			ind = -1;
			if(final[i]==final[1]) {
				ar[i] = 1;
				from[i] = 0;
				continue;
			}

			for( j=i-1; j>0; j-- ) {
				// if(j==i)q=2;
				if( final[i] > final[j]) {
					// printf("%d %d\n",final[i],final[j] );
					ind = (1+ar[j]>q)?j:ind;
					q = max(q,1+ar[j]);
				}
			}
			ar[i] = q;
			from[i] = ind;
			if(q>maxN)maxN=q;
		}

		for( i=1; i<=n; i++ ) {

			printf("%d ",ar[i]);
		}printf("\n");

		for( i=1; i<=n; i++ ) {

			printf("%d ",from[i]);
		}printf("\n");

		printf("%d\n", maxN);

	}
	return 0;
}

