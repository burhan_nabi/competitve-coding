#include <iostream>
#include <vector>

using namespace std;

typedef vector< short> array;

class BigInt
{
public:
	array digits = array(100000,0);	
};

int main(int argc, char const *argv[])
{
	ios::sync_with_stdio(0);

	BigInt n;

	short a = 100;

	cout<<a<<endl;

	for (int i = 0; i < 10; ++i)
	{
		cout<<n.digits[i];
	}
	cout<<endl;
	return 0;
}