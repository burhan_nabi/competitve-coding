#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 
typedef long long ll;

#define sz(a) 			int((a).size()) 
#define pb 				push_back 
#define all(c) 			(c).begin(),(c).end() 
#define tr(c,i) 		for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) 		for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) 	((c).find(x) != (c).end()) 
#define cpresent(c,x) 	(find(all(c),x) != (c).end()) 
#define f(i,a,b) 		for (int i = a; i < b; ++i)
#define rf(i,b,a) 		for (int i = b; i >= a; --i)



int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);
	
	int t;
	vi A(1000002);

	int n, maxNC, best_sum, current_sum, val;
	
	cin>>t;


	while(t--) {
		
		maxNC=0;best_sum=0;current_sum=0;val=0;

		cin>>n;
		
		f(i,0,n) {
			cin>>A[i];
		}

		// int max = -10001;
		f(i,0,n) {

			// if( A[i] > max) max=A[i];
			if( A[i] > 0) maxNC+=A[i];

			val = current_sum + A[i];
			
			if( val < 0) {
				if(best_sum==0) {
					best_sum = val;
				}
				current_sum = 0;
			}
			else {
				current_sum = current_sum + A[i];
				if( current_sum > best_sum ) {
					best_sum = current_sum;
				}
			}
		}

		if(maxNC==0) {
			cout<<best_sum<<" "<<best_sum<<endl;
			continue;
		}
		cout<<best_sum<<" "<<maxNC<<endl;
	}

	return 0;
}

