#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)



int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	int n;
	string s;

	cin>>n>>s;
	map<char, long long> dic;

	tr( s, i) {

		if(present(dic, *i)) {
			dic[*i]++; 
		}else {
			dic[*i] = 1;
		}
		
	}

	long long sum=0, temp=0;

	tr( dic, i) {
		// cout<<i->second<<endl;
		temp = ((i->second)*(i->second+1))/2;
		sum += temp;
	}

	cout<<sum<<endl;
	return 0;
}