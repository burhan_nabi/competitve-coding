
/*
Learned:
	LIS Quadratic
	LIS N lg N

	lower_bound(start, end, element, comparator_function)
	--->returns iterator to the first element which does
		not compare less than the passed element
	

	Why std::sort() is faster than C's qsort?
	--->
		Because we can pass function objects as comparators 
		and function objects can be inlined, unlike function
		pointers. 
*/


#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)


/*int comp(const ii &p1, const ii &p2) {

	if( p1.second != p2.second )
		return p1.second -p2.second;
	else 
		return p1.first -p2.first;
}*/

/*struct smallerPair
{
	inline bool operator() (const ii &p1, const ii &p2) 
	{
		if( p1.second != p2.second )
			return p1.second < p2.second;

		else 
			return p1.first < p2.first;
	
	}
};*/
struct smallerPair
{
	inline bool operator() (const ii &p1, const ii &p2) 
	{
		if( p1.first != p2.first )
			return p1.first < p2.first;

		else 
			return p1.second > p2.second;
	}
	
};

/*int pairOK(ii p1, ii p2) {
	if(p1.second > p2.second) {
		if(p1.first >= p2.first)
			return 1;
	}
	return 0;
}*/

int main(int argc, char const *argv[])
{
	// To make cin/cout (almost) as fast as scanf/printf
	ios::sync_with_stdio(false);

	int n;
	cin>>n;

	vector<ii > pairs(n);

	f(i,0,n) 
	{
		cin>>pairs[i].first;
		cin>>pairs[i].second;		
	}


	sort(all(pairs),smallerPair());
	
	// tr( pairs, i) 
	// {
	// 	cout<<i->first<<" "<<i->second<<endl;
	// }

	ii tp;
	tp.first = 0;tp.second = 0;
	int ans = 0, max = 0;
	
	/*tr( pairs, i) 
	{
		// cout<<i->first<<":"<<i->second<<endl;

		if(pairOK(*i,tp))
		{
			ans++;
			tp = *i;
			// cout<<tp.first<<" "<<tp.second<<endl;		
		}
	}	*/

	
/*
	O(n^2) longest incresing subsequence
	vi DP(n);	
	DP[0]=1;	
	f(i,1,n) 
	{
		DP[i] = 1;
		
		rf(j,i-1,0)
		{
			if(DP[j]+1 > DP[i] && pairs[i].second > pairs[j].second)
			{
				DP[i] = DP[j] + 1;
 				if(DP[i]>ans)ans=DP[i];
			}
		}
	}	

*/

	// O(N*lg(N)) longest increasing subsequence

	vi LIS;
	vi::iterator it;

	f(i,0,n)
	{
		it = lower_bound(all(LIS),pairs[i].second);
		if(it==LIS.end())
		{
			LIS.pb(pairs[i].second);
		}else
		{
			*it = pairs[i].second;
		}
	}

	ans = sz(LIS);
	cout<<ans<<endl;
	return 0;
}