#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)

	
int n, k;


int pairOK(int a, int b) {

	int d = a - b;
	if(d>k) {
		return 0;
	}else {
		if(d>=-k)return 1;
	}
	return 0;
}

int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	cin>>n>>k;

	vi girls(n), boys(n);

	f(i, 0, n) {
		cin>>girls[i];
	}

	f(i, 0, n) {
		cin>>boys[i];
	}	

	sort(all(girls));
	sort(all(boys));

	
	int ans=0;

	for(int i=0, j=0; i<n && j<n; ) {

		// cout<<girls[i]<<" "<<boys[j]<<endl;
		
		if (pairOK(girls[i],boys[j])) {
			
			ans++;
			i++;
			j++;
			continue;
		}
		else 
			if(girls[i] < boys[j])i++;
			else j++;
	}

	cout<<ans<<endl;
	return 0;
}