#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)

// #define m (unsigned int)1e9+7


int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	int n, q;
	cin>>n>>q;

	vector<long long> ar(n);
	vector<long long> toAdd(n+1);
	long long temp = 0;

	
	
	int t = 2;

	f(i,0,n+1) 
	{
		// cout<<"temp:"<<temp<<endl;
		temp +=  t;
		t += 2;
		toAdd[i] = temp%1000000007;
		// cout<<toAdd[i]<<" ";
	}

	

	int c, x, y;
	long long sum;
	while(q--) 
	{	
		cin>>c>>x>>y;
		switch(c) 
		{
			case 1:
				t = 0;
				f(i,x,y+1)
				{
					ar[i]=(ar[i]+toAdd[t++])%1000000007;
				}

				break;
			case 2:
				sum = 0;
				f(i,x,y+1)
				{
					sum += ar[i];
					sum %= 1000000007;
				}
				cout<<sum<<endl;
				break;
		}	
	}
	return 0;

}