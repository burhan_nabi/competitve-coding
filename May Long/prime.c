#include <stdio.h>

long long gcd (long long a, long long b) {
	if(b==0)return a;
	else return gcd(b,a%b);	
}


int main(int argc, char const *argv[])
{
	int t;
	long long a, b, g;

	scanf("%d",&t);

	while(t--) {
		scanf("%lld%lld",&a,&b);

		while(b!=1) {
			g = gcd(a,b);
			if(g==1) {
				printf("No\n");
				break;
			}
			b = b/g;
		}
		if(b==1) {
			printf("Yes\n");
		}
	}
	return 0;
}