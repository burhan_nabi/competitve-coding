#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int comp(const void *p1, const void *p2)
{
	int *i = (int *)p1;
	int *j = (int *)p2;

	return (*i) - (*j);
	
}

int main(int argc, char const *argv[])
{
	int t, i;
	int m = (int)(pow(10,9)+7), ans;

	long long power[100001];
	power[0] = 1;
	for( i=1; i<100001; i++){
		power[i] = (2 * power[i-1])%m;
	}
	
	scanf("%d",&t);

	
	while(t--) 
	{
		int n,i,j,k;
		scanf("%d",&n);
		int ar[n];
		for(i=0; i<n; i++){
			scanf("%d",&ar[i]);
		}


		qsort(&ar[0],n,sizeof(int),comp);
		
		long long plus=0, minus=0, temp;
		
		i=0, j=n-1;
		ans = 0;
		
		for(k=0; k<n; k++)
		{
			temp = ( (power[i]-1) * (ar[k])) %m;
			plus += temp;
			plus %=m;

			temp = ( (power[j]-1) * (ar[k])) %m;
			minus += temp;
			minus %=m;

			i++;j--;
		}
		
		ans = ((plus%m) - (minus%m))%m;
		if(ans < 0)ans += m; 
		printf("%d\n", ans);

	}	
	return 0;
}