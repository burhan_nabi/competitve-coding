#include <stdio.h>
#include <math.h>

long long powt[10000001];
long long A[10000001];

long long min(long long a , long long b){return (a<b)?a:b;}

int main(int argc, char const *argv[])
{
	int n, k, q;
	scanf("%d%d%d",&n,&k,&q);

	int a,b,c,d,e,f,r,s,t,m;
	scanf("%d%d%d%d%d%d%d%d%d%d%lld",&a,&b,&c,&d,&e,&f,&r,&s,&t,&m,&A[1]);	

	int la,lc,lm,da,dc,dm,l1,d1;
	scanf("%d%d%d%d%d%d%d%d",&l1,&la,&lc,&lm,&d1,&da,&dc,&dm);

	int i, j;

	powt[0] = 1;
	
	for(i=1; i<=n; i++) {
		powt[i] = (powt[i-1] * (t%s))%s;
	}
	
	
	long long temp,temp2;
	
	for(i=2; i<=n; i++) {
		temp2 = pow(A[i-1],2);
		temp = temp2%m;
		if(powt[i] <= r){
			A[i] = ((a%m * temp%m )%m + ( b%m * A[i-1]%m )%m + c%m)%m;
		}else
		{
			A[i] = ((d%m * temp%m )%m + ( e%m * A[i-1]%m )%m + f%m)%m;
		}
		printf("%lld\n",A[i]);
	}

	int L , R;
	long long minq ;
	long long sum = 0 , pro = 1;

	for( i=1; i<=q; i++) {
		temp = ( la%lm *  l1%lm ) % lm;
		l1 = ((temp%lm) + lc%lm )%lm;
		temp = (da%dm * d1%dm ) % dm;
		d1 = ((temp%dm) + dc%dm) % dm;

		L = l1 + 1;
		temp = L + k - 1 + d1;
		R = min(temp, n);	
		printf("%d %d\n",L,R );

		minq = 10000000008;
		if(k < (L-R)) {
			L = k;
			R = 2*k;
		}
		for(j = L; j<=R; j++) {
			if(A[j] < minq) minq = A[j]; 
		}
		sum += minq;
		pro *= minq;
		pro %= 1000000007; 
	}
	printf("%lld %lld\n",sum,pro);
	return 0;
}