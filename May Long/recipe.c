#include <stdio.h>



int main(int argc, char const *argv[])
{
	int t, n, i;

	scanf("%d",&t);
	
	while(t--)
	{
		scanf("%d",&n);

		int min=10001, sum=0, temp;
		
		for(i=0; i<n; i++)
		{
			scanf("%d",&temp);
			
			if(temp < 2)
			{
				printf("-1\n");
				while(i < n-1){
					i++;
					scanf("%d",&temp);
				}
				goto exit;
			}
			
			sum+= temp;
			if(temp < min)min = temp;

		}
		printf("%d\n",sum - min + 2 );
		
		exit:;
	}	

	return 0;
}