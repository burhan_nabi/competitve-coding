#include <stdio.h>

typedef struct node {
	int neighbours[200];
	int degree;;
} node;

int odd, even, k;
int visited[201] ={0};
int deducted[201] = {0};
int e[201] = {-1};
node city[201] = {0};
	
int this;

void visit(int i, int k) {
	visited[i] = 1;

	if(k%2) {
		odd++;
		e[i] = 0;
	}else {
		even++;
		e[i] = 1;
	}
	int n, j, d = city[i].degree;
	for(j=0; j<d; j++) {

		n = city[i].neighbours[j];
		if(!visited[n]) {
	
			if(k%2)odd--;
			else even--;

			visit(n,k+1);
		}else {
			if(e[i] == e[n] && !deducted[i] && !deducted[n]) {
				deducted[i] = deducted[n] = 1;
				printf("%d %d\n",i,n );
				if(e[1] == 1) {
					even--;
				}
				else odd--;
			}
		}
	}
}

int main(int argc, char const *argv[])
{
	int n, m;
	scanf("%d%d",&n,&m);
	
	int marked[201] = {0};
	// visited = {0}

	int edges = m;
	int cities = n;
	int d, i, from, to, j;
	while(edges --) {
		scanf("%d%d",&from,&to);
		city[from].neighbours[city[from].degree++] = to;
		city[to].neighbours[city[to].degree++] = from;
	}

	// for(i=1; i<=n; i++) {
	// 	printf("%d:%d\n",i,city[i].degree);
	// }

	int ans1 = 0, ans2=0, imm_neig, t = 0;

	for(i=1; i<=n ;i++) {

	/*	
		k = 0; odd= 0; even = 0;
		if(!visited[i]) {
			visit(i,k);
			// printf("%d %d\n",odd,even);
		}			

		t = (odd>even)?odd:even;
		ans1 += t;
	*/	
	
		if(city[i].degree == 0)ans2++;
		
		if(city[i].degree == 1 && !marked[i]) {
			ans2 +=1;
			marked[i]=1;
			imm_neig = city[i].neighbours[0];
			d = city[imm_neig].degree;

			for(j=0; j<d; j++) {

				to = city[imm_neig].neighbours[j];
				if( to == i)continue;
				if(city[to].degree == 1 && !marked[to]) {
					ans2 += 1;
					marked[to] = 1;
				}
			}
			ans2 -= 1;
		}

	}	
	// if(ans2 > ans1)printf("%d\n",ans2 );
	// else printf("%d\n",ans1);
	
	// if(ans2==0)printf("1\n");
	// else printf("%d\n", ans2);
	// printf("1\n");
	printf("%d\n", ans2);
	return 0;
}