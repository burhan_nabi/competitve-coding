#include <stdio.h>


char inline opp(char a) { return (a=='0')?'1':'0';}

int main(int argc, char const *argv[])
{
	int t, n, k;

	scanf("%d",&t);

	int i, j, count;

	char str[100001];
	while(t--) {
		count = 0;
		scanf("%d%d",&n,&k);
		scanf("%s",str);

		if(k == 1) {
			i = 0;
			int ch1=0, ch2=0;
			while(i < n) {
				if(i%2==0) {
					if(str[i]!='0')ch1++;
					else ch2++;
				}else {
					if(str[i]!='1')ch1++;
					else ch2++;
				}
				i++;
			}
			if(ch2 < ch1) {
				i=0;
				printf("%d\n",ch2 );
				while(i < n) {
					if(i%2==0)printf("1");
					else printf("0");
					i++;
				}
			}else {
				i=0;
				printf("%d\n", ch1);
				while(i < n) {
					if(i%2==0)printf("0");
					else printf("1");
					i++;
				}
			}
			printf("\n");
			continue;
		}

		for (i=0; i<n-1; ++i) {
			
			j = 0;
			while(str[i]==str[i+1] && i<n-1) {
				
				j++;
				i++;
				if( j == (2*k)) {
					str[i-k] = opp(str[i]);
					i = i- k;
					j=0;
					count++;
					break;
				}
				// printf("while j:%d\n",j);
			}
			
			if(j >= k) {
				str[i + k - j - 1] = opp(str[i]);
				i -= 1; 
				count++;
				// printf("Sec\n");
			}

		}
		printf("%d\n%s\n",count,str);

	}
	return 0;
}