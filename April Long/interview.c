#include <stdio.h>
#include <math.h>

int main(int argc, char const *argv[])
{
	int t, n, i;
	int fac1, fac2;

	long long ans;

	scanf( "%d", &t );

	while( t-- ) { 
		ans = 0;			
		scanf( "%d", &n );
		
		if(n==1){
			printf("1\n");
			continue;
		}
		
		ans += n + 1;
		for (i=2; i<(int)sqrt(n) + 1; i++) {
			
			if(n%i == 0) {
				
				fac1 = i;
				fac2 = n/i;

				if(fac1 == fac2) {
					ans += fac1;
					continue;			
				}
				ans += fac2 + fac1;
			}

		}

		printf("%lld\n", ans);
	}
	return 0;
}