#include <stdio.h>

int main(int argc, char const *argv[])
{
	int t , n , prev, Mis, temp , ans;

	scanf( "%d", &t );

	while( t-- ) { 			
		ans = 0;
		scanf( "%d", &n );

		scanf( "%d", &prev );
		
		Mis = 0;

		while( --n ) { 		
			scanf( "%d", &temp );
			
			if(temp == prev){Mis = 0;prev = temp;continue;}
			
			if (Mis) {
				ans ++;
				Mis = 1;
				prev = temp;
				continue;
			}
			prev = temp;
			ans +=2;
			Mis = 1;
		}

		printf("%d\n", ans);
	}
	return 0;
}