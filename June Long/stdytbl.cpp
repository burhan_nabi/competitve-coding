#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 

#define m (long long)1e9

int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	vvi mat;
	vi row(2002);

	vvi temp;

	for (int i = 0; i < 2002; ++i)
	{
		mat.pb(row);
		temp.pb(row);
	}
	// mat[2][1] = 1;
	for (int i = 1; i < 2002; ++i)
	{
		mat[0][i] = 1;
	}
	for (int i = 1; i < 2002; ++i)
	{
		mat[i][i] = mat[i-1][i+1];
		for (int j = i+1; j < 2002; ++j)
		{
			mat[i][j] = ((mat[i-1][j])%m + (mat[i][j-1]%m))%m;
		}
	}

	// for (int i = 0; i < 15; ++i)
	// {
		
	// 	for (int j = 0; j < 15; ++j)
	// 	{
	// 		cout<<mat[i][j]<<" ";
	// 		// cout<<temp[i][j]<<" ";
	// 	}
	// 	cout<<endl;
	// }
	
	/* Stdy Table*/

	int t, x, y;	
	cin>>t;


	long long var;

	while(t--) {

		cin>>x>>y;

		for (int i = 0; i <= y; ++i)
		{
			temp[1][i] = mat[i][y+1];
		}

		for (int i = 0; i <= x; ++i)
		{
			temp[i][0] = 1;
		}

		for (int i = 2; i <= x; ++i)
		{
			
			for (int j = 1; j <=y; ++j)
			{
				var = (((long long)mat[j][y]) * ((long long)temp[i-1][j]));
				var %= m;
				var += (long long)temp[i][j-1];
				var %= m;
				temp[i][j] = var;
			}

		}

		cout<<temp[x][y]<<endl;
		// for (int i = 0; i < 15; ++i)
		// {
			
		// 	for (int j = 0; j < 15; ++j)
		// 	{
		// 		// cout<<mat[i][j]<<" ";
		// 		cout<<temp[i][j]<<" ";
		// 	}
		// 	cout<<endl;
		// }

	}

	return 0;
}