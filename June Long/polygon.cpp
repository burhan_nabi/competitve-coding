#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 

int main(int argc, char const *argv[])
{
	// ios::sync_with_stdio(false);

	map<int,int> M;
	vi ans;

	int t, pols, n, temp, x, y, max;

	scanf("%d",&t);

	while( t-- ) {
		
		scanf("%d",&pols);
		ans.clear();
		ans.resize(pols);
		M.clear();

		for (int i = 0; i < pols; ++i)
		{
			
			scanf("%d",&n);
			max = -1000000;
			while( n-- ) {

				scanf("%d%d",&x,&y);
				if(x > max) max = x;
			}
			M[max] = i ;
		}
		int i = 0;
		
		tr(M, it)
			ans[it->second] = i++;

		tr(ans, it)
			cout<<*it<<" ";
	}


	return 0;
}