#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 


#define m (int) 99991

template <typename Iterator>
inline bool next_combination(const Iterator first, Iterator k, const Iterator last)
{
   if ((first == last) || (first == k) || (last == k))
      return false;
   Iterator itr1 = first;
   Iterator itr2 = last;
   ++itr1;
   if (last == itr1)
      return false;
   itr1 = last;
   --itr1;
   itr1 = k;
   --itr2;
   while (first != itr1)
   {
      if (*--itr1 < *itr2)
      {
         Iterator j = k;
         while (!(*itr1 < *j)) ++j;
         iter_swap(itr1,j);
         ++itr1;
         ++j;
         itr2 = k;
         rotate(itr1,j,last);
         while (last != j)
         {
            ++j;
            ++itr2;
         }
         rotate(k,itr2,last);
         return true;
      }
   }
   rotate(first,k,last);
   return false;
}


int main(int argc, char const *argv[])
{
	int n, k;
	long long ans = 0;

	vi fib(50005);

	fib[1] = 1;
	fib[2] = 1;

	// cout<<fib[0]<<fib[1]<<fib[2];
	for (int i = 3; i < 50001; ++i)
	{
		ans = fib[i-2] + fib[i-1];
		// cout<<ans;
		fib[i] = ans%m;
	}

	// for (int i = 0; i < 10; ++i)
	// {
	// 	cout<<fib[i];
	// }

	cin>>n>>k;
    
    vi set(n);

    for (int i = 0; i < n; ++i)
    	cin>>set[i];

    long long sum;
    ans = 0;
	do
	{
		sum = 0;
	   for (int i = 0; i < k; ++i)
	   {
	      // std::cout << set[i];
	      sum += (long long)set[i]%m;
	      sum %= m;

	   }
	   ans += fib[sum];
	   ans %= m;
	}
	while(next_combination(set.begin(),set.begin() + k,set.end()));

	cout<<ans<<endl;
	return 0;
}