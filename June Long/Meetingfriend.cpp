#include <iostream>
#include <iomanip>
#include <cmath>


using namespace std;

double calcArea(double a, double b, double c, double d) {

	double small_area, large_area, total, m, n, shaded_a;
	total = a*b;
	shaded_a = 0;

	small_area = pow(a,2)/2;
	large_area = total - small_area;
	shaded_a = (d>=a)?(shaded_a + small_area):shaded_a + small_area - (pow((a-d),2)/2);
	shaded_a = (c>=b)?(shaded_a + large_area):((a+c>=b)?(shaded_a + large_area - (pow((b-c),2)/2)):shaded_a + large_area -(pow(a,2))/2 - ((b-a-c)*a));
	return shaded_a/total;
}


int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	int t, T1,T2,t1,t2;
	
	cin>>t;
	
	cout<<fixed<<setprecision(8);
		
	while(t--) {
		
		cin>>T1>>T2>>t1>>t2;
		double ans = (T1<=T2?(calcArea(T1,T2,t1,t2)):(calcArea(T2,T1,t2,t1)));
		cout<<ans<<endl;
	}

}