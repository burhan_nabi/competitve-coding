#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 

#define m (int)1e9+7


int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);
	int t, q, n;


	vvi mat(5002);
	vi row(5002);


	tr( mat, i) {
		*i = row;
	}

	for (int i = 0; i < 5002; ++i)
	{
		mat[i][0] = 0;
	}

	mat[1][1]=mat[1][2] =1;
	for (int i = 2; i < 5002; ++i)
	{
		
		for (int j = 1; j <= i+1; ++j)
		{
			
			mat[i][j] = (mat[i-1][j-1]+mat[i-1][j])%m;
		}
	}


	// for (int i = 1; i < 10; ++i)
	// {
	// 	for (int j = 1; j < 10; ++j)
	// 	{
	// 		cout<<mat[i][j]<<" ";
	// 	}
	// 	cout<<endl;
	// }

	cin>>t;
	string s;

	while(t--) {
		
		cin>>n>>q;
		cin>>s;

		int j=0, len=sz(s) ;

		// vector<string> sv(len);

		// tr(sv, i) {
		// 	*i = s.substr(j++,len-j);
		// }

		// vi count(len);

		map<char, int> M;

		tr( s, i) {

			if(present(M,*i)) {
				M[*i]++;
			}else {
				M[*i] = 1;
			}
		}

		tr( M, i) {
			cout<<i->first<<":"<<i->second<<endl;
		}




	}
	mat.clear();	
	return 0;
}

