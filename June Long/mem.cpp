#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 



int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	long long n,prev,temp,ans, t;
	cin>>t;
	while(t--)	{
		cin>>n;
		prev = 0,temp = 0,ans = 0;
		
		while(n--) {
			cin>>temp;
			if(temp > prev) {
				ans += temp-prev;
			}
			prev = temp;
		}
	
		cout<<ans<<endl;
	}	

	return 0;
	
}

