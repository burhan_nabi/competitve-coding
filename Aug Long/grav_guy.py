def calcJumps(l, r, choice):
	count = 0
	if choice==1:
		current,other = l,r
		for i in range(0, len(l)-1 ):
			# print i,current
			if( current[i+1]=='.'):continue
			elif(other[i+1]=='.'):
				count += 1
				i+=1
				current,other = other,current
			else:
				count += 1
				i -= 1
				current,other = other,current
		return count
	else:
		return calcJumps(r,l,1)

t = input()
while t:
	t -= 1
	l = raw_input()
	r = raw_input()

	flag = 0	
	
	for i in range( len(l) ):
		if (l[i]!='.' and r[i]!='.'):
			print "No"
			flag = 1		
			break
	if flag==1:
		continue

	print "Yes"
	
	# Calculate Min Jumps
	if( l[0]=='.' and r[0]!='.' ):
		print calcJumps(l, r, 1)
		continue
	elif( l[0]!='.' and r[0]=='.' ):
		print calcJumps(l, r, 2)
		continue
	else:
		left = calcJumps(l, r, 1)
		right = calcJumps(l, r, 2)
		if left>right:
			print right
		else:
			print left	 