#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)

vi row(1000001);

int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	int t;
	cin>>t;
	int n, a, b, h, j, prev;
	int max = 0;

	int min = 1000002, temp, ans = -1;

	while(t--) 
	{
		// row.clear();
		cin>>n>>h;
		max = 0;
		ans = 0;
		temp = n;

		f(i,0,n) {
			row[i] = 0;
		}

		while(temp--)
		{
			cin>>a>>b;
			
			f(i,a,b+1)
				row[i]++;	
		}

		// Init
		f(i, 0, h) {
			// cout<<row[i]<<endl;
			max += row[i];
			// sum[i] = row[i];
		}

		// cout<<max<<endl;

		f( i, h, n )
		{
			// cout<<max<<" Ans:"<<ans<<endl;
			// temp = max;
			prev = row[i-h];
			max -= prev;
			max += row[i];
			if(max > ans)
				ans = max;
		}
		if(max>ans)
			ans = max;
		// cout<<"N:"<<n<<" H:"<<h<<"h*n:"<<h*n;
		ans = (h*n) - ans;

		cout<<ans<<endl;
	}
	return 0;
}

