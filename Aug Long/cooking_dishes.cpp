#include <iostream>

using namespace std;

int powers[25];
int reduced = 0;

int reduce(int a, int b, int count ) {
	if(a==b)return count;
	count++;
	return a%2? reduce( (a-1)/2 , b, count): reduce(a/2, b, count);
}

int isAPowerOfTwo(int a){
	for (int i = 0; i < 25; ++i)
	{
		if(powers[i]>a)
			return 0;
		else if(powers[i]<a)
			continue;
		else return 1;
	}
}

int reduceToPowerOfTwo(int a, int count) {
	if( isAPowerOfTwo(a) ) {
		reduced = a;
		return count;
	}
	else
	{
		if(a%2)
			return reduceToPowerOfTwo( (a-1)/2, count+1);
		else
			return reduceToPowerOfTwo( a/2, count+1);			
	}
}

int increase(int a, int b, int count) {
	for (int i = 0; i < 25; ++i)
	{
		if(powers[i]==b)
			return count;
		if( powers[i] >= a)
			count++;
	}
}

int main(int argc, char const *argv[])
{
	ios::sync_with_stdio(false);

	powers[0]=1;
	for (int i = 1; i < 25; ++i)
		powers[i] = 2*powers[i-1];

	int t, a, b;
	cin>>t;

	while(t--) {
		cin>>a>>b;
		if(a > b) {
			int final;
			int init = reduceToPowerOfTwo(a, 0);
			if( reduced < b )
				final = increase(reduced, b, 0);
			else
				final = reduce(reduced, b, 0);
			cout<<init + final<<endl;

		}else {
			int init = reduceToPowerOfTwo(a, 0);
			int final = increase(reduced, b, 0);
			cout<<init+final<<endl;
		}
	}
	return 0;
}