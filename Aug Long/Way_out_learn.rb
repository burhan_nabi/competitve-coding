t = gets.chomp.to_i
# rows = Array.new(1000000,0)		
t.times do
	
	n , h = gets.chomp.split().map { |e| e.to_i  }
	rows = Array.new(n+2,0)
	n.times do
		a , b = gets.chomp.split().map { |e| e.to_i }
		rows[a+1] -= 1
		rows[b+2] += 1
	end

	rows[0] = n
	
	(n+1).times do |i|
		rows[i+1] += rows[i] 
	end
	
	(n+1).times do |i|
		rows[i+1] += rows[i] 
	end

	min = 10000000
	(h..n+1).each do |i|
		if((rows[i] - rows[i-h])<min)
			min = rows[i] - rows[i-h]
		end
	end

	puts min

end
