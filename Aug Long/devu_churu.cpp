#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <string>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)



int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);

	int n, m;
	cin>>n>>m;

	vi A(n);
	string choice, starter;
	int k, num; 

	f(i, 0, n)
		cin>>A[i];
	
	sort( all(A) );
	

	vi::iterator low,up;

	f(i, 0, m) {
		cin>>choice>>k>>starter;
		low = lower_bound(all(A), k);
		up = upper_bound(all(A),k);
		// cout<<low - A.begin()<<" "<<up - A.begin()<<endl;
		if( choice.compare("<") == 0 ) {
			num = low - A.begin();
			cout<<"num: "<<num<<endl;
		}else if( choice.compare(">") == 0 ) {
			num = A.end() - up;
		}else {
			num = up - low;
		}
	}
	return 0;
}