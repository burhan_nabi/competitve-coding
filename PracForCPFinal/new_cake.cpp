#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cstdlib>

using namespace std;

typedef vector<int > vi; 
typedef vector<long long > vll; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)

long long powt[10000001];
vll A(10000001);
vll mins;



int main(int argc, char const *argv[])
{
	int n, k, q;
	scanf("%d%d%d",&n,&k,&q);

	int a,b,c,d,e,f,r,s,t,m;
	scanf("%d%d%d%d%d%d%d%d%d%d%lld",&a,&b,&c,&d,&e,&f,&r,&s,&t,&m,&A[1]);	

	int la,lc,lm,da,dc,dm,l1,d1;
	scanf("%d%d%d%d%d%d%d%d",&l1,&la,&lc,&lm,&d1,&da,&dc,&dm);

	int i, j;

	powt[0] = 1;
	
	for(i=1; i<=n; i++) {
		powt[i] = (powt[i-1] * (t%s))%s;
	}
	
	
	long long temp,temp2;
	
	for(i=2; i<=n; i++) {
		temp2 = pow(A[i-1],2);
		temp = temp2%m;
		if(powt[i] <= r){
			A[i] = ((a%m * temp%m )%m + ( b%m * A[i-1]%m )%m + c%m)%m;
		}else
		{
			A[i] = ((d%m * temp%m )%m + ( e%m * A[i-1]%m )%m + f%m)%m;
		}
	}

	// f(i,1,n+1)cout<<A[i]<<" ";
	// cout<<endl;

	int lenOfWindow = k;
	
	vi window(lenOfWindow);

	f(i,1,lenOfWindow+1)
		window[i-1] = A[i];

	sort(all(window));		

	f(i, lenOfWindow+1, n+1) {		
		mins.pb( window.front() );

		while( A[i] < window.back() )
			window.pop_back();

		window.pb(A[i]);
		
		if( A[i - lenOfWindow] == window.front() )
			window.erase( window.begin(), window.begin()+1 );

	}
	mins.pb(window.front());
			
	// tr( mins, i) {
	// 	cout<<*i<<" ";
	// }
	// cout<<endl;

	int L , R;
	long long minq ;
	long long sum = 0 , pro = 1;

	for( i=1; i<=q; i++) {
		temp = ( la%lm *  l1%lm ) % lm;
		l1 = ((temp%lm) + lc%lm )%lm;
		temp = (da%dm * d1%dm ) % dm;
		d1 = ((temp%dm) + dc%dm) % dm;

		L = l1 + 1;
		temp = L + k - 1 + d1;
		R = temp>n ? n : temp;	

		// printf("%d %d\n",L,R );


		//Find minq
		// cout<<L<< " "<<R<<endl;
		// cout<<mins[L-1]<< " "<<mins[R - lenOfWindow];
		minq = mins[L-1] < mins[R - lenOfWindow] ? mins[L] : mins[R - lenOfWindow];
		sum += minq;
		pro *= minq;
		sum %= 1000000007;
		pro %= 1000000007; 
	}
	printf("%lld %lld\n",sum,pro);

	return 0;
}