#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cstdlib>

using namespace std;

typedef vector<int > vi; 
typedef vector<vi> vvi; 
typedef pair<int,int> ii; 

#define sz(a) int((a).size()) 
#define pb push_back 
#define all(c) (c).begin(),(c).end() 
#define tr(c,i) for(auto i = (c).begin(); i != (c).end(); i++) 
#define ntr(c,i) for(auto i = (c)->begin(); i != (c)->end(); i++) 
#define present(c,x) ((c).find(x) != (c).end()) 
#define cpresent(c,x) (find(all(c),x) != (c).end()) 
#define f(i,a,b) for (int i = a; i < b; ++i)
#define rf(i,b,a) for (int i = b; i >= a; --i)



int main(int argc, char const *argv[])
{
	// To make cin/cout as fast as scanf/printf
	ios::sync_with_stdio(false);
	srand(time(NULL));
	vi arr(20);
	vi min;
	tr( arr, i) {
		*i = rand()%100;
		cout<<*i<<" ";
	}
	cout<<endl;

	int lenOfWindow;
	cin>>lenOfWindow;
	
	vi window(lenOfWindow);

	f(i,0,lenOfWindow)
		window[i] = arr[i];

	sort(all(window));		

	f(i, lenOfWindow, sz(arr)) {
		
		min.pb( window.front() );

		while( arr[i] < window.back() )
			window.pop_back();

		window.pb(arr[i]);

		
		if( arr[i - lenOfWindow] == window.front() )
			window.erase( window.begin(), window.begin()+1 );

	}
	min.pb(window.front());
		
	tr( min, i) {
		cout<<*i<<" ";
	}
	cout<<endl;

	return 0;
}
